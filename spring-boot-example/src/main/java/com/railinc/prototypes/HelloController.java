package com.railinc.prototypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Autowired
	private ApplicationContext context;
	
	@RequestMapping("/")
	public String index() {
		return "Index!";
	}
	
	@RequestMapping("/beans")
	public String beans() {
		
		StringBuilder beans = new StringBuilder();
		for(String bean : context.getBeanDefinitionNames()) {
			beans.append(String.format("%s</br>", bean));
		}
		return beans.toString();
	}
}
